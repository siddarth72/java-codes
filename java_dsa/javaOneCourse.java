// import java.util.*;

public class javaOneCourse {
    public static void main(String[] args) {

    }
}

// ----------------------------------------------------------------

// ">>" RIGHT SHIFT: DIVIDE BY 2
// "<<" LEFT SHIFT: MULTIPLY BY 2

// TRAILING 0'S IN A FACTORIAL:
// int n = 25;
// int res = 0;
// for (int i = 5; i <= n; i = i * 5) {
// res += (n / i);
// }
// System.out.println(res);

// SIEVE OF ERATOSTHENES:
// boolean isPrime[] = sieveOfEratosthenes(20);
// for (int i = 0; i <= 20; i++) {
// System.out.println(i + " " + isPrime[i]);
// }
// }
// static boolean[] sieveOfEratosthenes(int n) {
// boolean isPrime[] = new boolean[n + 1];
// Arrays.fill(isPrime, true);
// isPrime[0] = false;
// isPrime[1] = false;
// for (int i = 2; i * i <= n; i++) {
// for (int j = 2 * 1; j <= n; j += i) {
// isPrime[j] = false;
// }
// }
// return isPrime;

// GCD(HCF):
// static int gcd(int a, int b) {
// return a % b == 0 ? b : gcd(b, a % b);
// }
// System.out.println(gcd(15, 27));

// FAST POWER:
// static int fastPower(int a, int b) {
// int res = 1;
// while (b > 0) {
// if ((b & 1) != 0) {
// res = res * a;
// }
// a = a * a;
// b = b >> 1;
// }
// System.out.println(res);
// }

// MODULO ARITHMETICS:
// static long fastPower(long a, long b, int n) {
// long res = 1;
// while (b > 0) {
// if ((b & 1) != 0) {
// res = (res * a % n) % n;
// }
// a = (a % n * a % n) % n;
// b = b >> 1;
// }
// return res;
// System.out.println(fastPower(375236, 5, 1000000007));

// RECURSION:
// Find the sum of first n natural numbers
// static int sum(int n) {
// if (n == 1)
// return 1;
// return n + sum(n - 1);
// }
// System.out.println(sum(10));

// static int POWER(int a, int b) {
// if (b == 0)
// return 1;
// if (b == 1)
// return a;
// return a * POWER(a, b - 1);
// }
// System.out.println(POWER(2, 3));

// NUMBER OF WAYS IN n x m MATRIX:
// static int count(int n, int m) {
// if (n == 1 || m == 1)
// return 1;

// return count(n - 1, m) + count(n, m - 1);
// }
// System.out.println(count(3, 3));

// JOSEPHUS PROBLEM:
// static int joseph(int n, int k) {
// if (n == 1)
// return 0;
// return (joseph(n - 1, k) + k) % n;
// }
// System.out.println(joseph(5, 3));

// STRING RECURSION:
// PALINDROME STRING
// static boolean isPalindrome(String s, int l, int r) {
// if (l >= r)
// return true;
// if (s.charAt(l) != s.charAt(r))
// return false;

// return isPalindrome(s, l + 1, r - 1);
// }
// String str = "RACECAR";
// int i = 0;
// int j = 6;
// System.out.println(isPalindrome(str, i, j));

// BACKTRACKING:
// Q -> N-QUEEN PROBLEM:
// boolean isSafe(int board[][], int row, int col) {
// final int n = 4;
// int i, j;
// for (i = 0; i < col; i++) {
// if (board[row][i] == 1) {
// return false;
// }
// }
// for (i = row, j = col; i >= 0 && j >= 0; i--, j--) {
// if (board[i][j] == 1) {
// return false;
// }
// }
// for (i = row, j = col; j >= 0 && i < n; i++, j--) {
// if (board[i][j] == 1) {
// return false;
// }
// }
// return true;
// }
// boolean nQueen(int board[][], int col) {
// final int n = 4;
// if (col >= n)
// return true;
// for (int i = 0; i < n; i++) {
// if (isSafe(board, i, col)) {
// board[i][col] = 1;
// if (nQueen(board, col + 1) == true) {
// return true;
// }
// board[i][col] = 0;
// }
// }
// return false;
// }

// ----------------------------------------------------------------

// PROBLEMS:

// ->PALINDROME
// int n = 125;
// int m = 0;
// // int p = 0;
// while (n >= 0) {
// int k = n % 10;
// m = k;
// n /= 10;
// m = k * 10 + m;
// }
// System.out.println(n);

// -> POWERSET OF A GIVEN STRING:
// static String powerSet(String s, int i, String cur) {
// if (i == s.length()) {
// return cur;
// }
// return powerSet(s, i + 1, cur + s.charAt(i));
// return powerSet(s, i + 1, cur);
// }

// ALL PERMUTATIONS OF A GIVEN STRING