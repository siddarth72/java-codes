
public class java2o {
    public static void main(String[] args) throws Exception {
        int a[][] = { { 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1, 0, 0 }, { 1, 0, 0, 1, 1, 0, 1, 1 },
                { 1, 2, 2, 2, 2, 0, 1, 0 }, { 1, 1, 1, 2, 2, 2, 2, 0 }, { 1, 1, 1, 1, 1, 2, 1, 1 },
                { 1, 1, 1, 1, 1, 2, 2, 1 } };
        System.out.println("Original Array");
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        floddFill(a, 4, 3, 3, 5);
        System.out.println();
        System.out.println("Modified Array");
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }

    }

    static void floddFill(int a[][], int r, int c, int toFill, int prevFill) {
        int rows = a.length;
        int cols = a[0].length;
        if (r < 0 || r >= rows || c < 0 || c >= cols) {
            return;
        }
        if (a[r][c] != prevFill) {
            return;
        }
        a[r][c] = toFill;
        floddFill(a, r - 1, c, toFill, prevFill);
        floddFill(a, r, c - 1, toFill, prevFill);
        floddFill(a, r + 1, c, toFill, prevFill);
        floddFill(a, r, c + 1, toFill, prevFill);
    }

}
