// import java.util.LinkedList;

class myll<E> {
    Node<E> head;

    public void add(E data) {
        Node<E> toadd = new Node<E>(data);
        if (head == null) {
            head = toadd;
            return;
        }
        Node<E> temp = head;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = toadd;
    }

    void print() {
        Node<E> temp = head;
        while (temp != null) {
            System.out.print(temp.data + " ");
            temp = temp.next;
        }
    }

    public boolean isEmpty() {
        return head == null;
    }

    public E removelast() throws Exception {
        Node<E> temp = head;
        if (temp == null) {
            throw new Exception("Empty stack");
        }
        if (temp.next == null) {
            Node<E> toRemove = head;
            head = null;
            return toRemove.data;
        }
        while (temp.next.next != null) {
            temp = temp.next;
        }
        Node<E> toRemove = temp.next;
        temp.next = null;
        return toRemove.data;
    }

    public E getlast() throws Exception {
        Node<E> temp = head;
        if (temp == null) {
            throw new Exception("Empty stack");
        }
        while (temp.next != null) {
            temp = temp.next;
        }
        return temp.data;
    }

    static class Node<E> {
        E data;
        Node<E> next;

        public Node(E data) {
            this.data = data;
            next = null;
        }
    }
}

public class ll {
    public static void main(String[] args) {
        // List<Integer> ll = new LinkedList<Integer>();
        // List<Integer> al = new ArrayList<Integer>();
        myll<Integer> ll = new myll<Integer>();
        ll.add(72);
        ll.add(73);
        ll.print();
    }
}
